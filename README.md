---
title: Dotfiles
author: qkx
---

# My Dotfiles

Personal dotfiles maintained for frequently used tools used in Linux, allowing configurations to be easily set-up on a fresh debian-based install.

Configuration for tools include:

* vim
    * Minimal vimrc
* tmux (version 3.0a)
    * `C-a` as prefix instead of default `C-b`
    * Split panes with `Prefix + |/-`
    * Split panes use current path directory
    * Window/pane navigation using vim bindings
* alacritty
* bash_aliases
    * **ll** - List directories first and then files
    * **la** - List all directories except `.` and `..`
* scripts
    * **update-os.sh** - Update packages
    * **install-packages.sh** - Install dependencies needed for dotfiles

## Usage

1. Clone repo
1. Run `setup` script in main directory for full setup

Alternatively:

1. Run `setup` script in subdirectories for modular setup

Note: Requires running script with `sudo` to install packages.

```bash
git clone https://gitlab.com/qkx/dotfiles
./setup
```
