---
author: Isa Haron
---

# Vim Reference Sheet

This document serves as a reference for useful `vim` commands found from reading through the book `Practical Vim` to be incorporated into a personal workflow.

## Insert mode

### Edit text while in Insert mode

* `<C-h>` - Delete back one character (backspace)
* `<C-w>` - Delete back one word
* `<C-u>` - Delete back to start of line (think undo)

### Switch from Insert mode

* `<C-o>` - Switch to Insert Normal mode
    * Fire off single command before switch to Insert mode

### Paste from Insert mode

* `<C-r>0` - Paste newly yanked text at current cursor position

### Calculations in Insert mode

* `<C-r>=`6*35`<CR>` - Calculate `6 * 35` and output as text

### Insert character by Character Code

* `<C-v>{code}`
    * `ga` - find out Character Code of character under cursor

### Overwrite with Replace Mode

* `gr{char}` - one-shot replace and switch to Normal mode

## Visual Mode

### Previous visual selection

* `gv`- Visual select previous selection made

### Toggle Free End of Selection

* `o` - While in Visual mode, toggle cursor to either end of selection

### Convert selection to Uppercase

* `U` - With selected text, convert text to uppercase
    * `gU{motion}` - Normal mode equivalent
