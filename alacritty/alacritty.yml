# Configuration for Alacritty, the GPU enhanced terminal emulator.

env:
    TERM: xterm-256color

window:
  dimensions:
    columns: 0
    lines: 0

  position:
    x: 0
    y: 0

  padding:
    x: 5
    y: 5

  dynamic_padding: false

  decorations: none

  startup_mode: Maximized

#scrolling:
  # Maximum number of lines in the scrollback buffer.
  # Specifying '0' will disable scrolling.
  #history: 10000

  # Scrolling distance multiplier.
  #multiplier: 3

# Font configuration
font:
  normal:
    family: Fira Mono

    style: Semibold

  bold:
    family: Fira Mono

    style: Bold

  italic:
    family: Fira Mono

    style: Italic

  bold_italic:
    family: Fira Mono

    style: Bold Italic

  size: 11.0

  offset:
    x: 0
    y: 0

  glyph_offset:
    x: 0
    y: 0

draw_bold_text_with_bright_colors: false

# Colors (Blood Moon)
colors:
  # Default colors
  primary:
    background: '#10100E'
    foreground: '#C6C6C4'

  # Normal colors
  normal:
    black:   '#10100E'
    red:     '#C40233'
    green:   '#009F6B'
    yellow:  '#FFD700'
    blue:    '#0087BD'
    magenta: '#9A4EAE'
    cyan:    '#20B2AA'
    white:   '#C6C6C4'

  # Bright colors
  bright:
    black:   '#696969'
    red:     '#FF2400'
    green:   '#03C03C'
    yellow:  '#FDFF00'
    blue:    '#007FFF'
    magenta: '#FF1493'
    cyan:    '#00CCCC'
    white:   '#FFFAFA'

bell:
  animation: EaseOutExpo
  duration: 0
  color: '#ffffff'

# # Colors (Flat remix)
# colors:
#   # Default colors
#   primary:
#     background: "#1F2229"
#     foreground: "#E6E6E6"

#   # Normal colors
#   normal:
#     black: "#1F2229"
#     red: "#D41919"
#     green: "#5EBDAB"
#     yellow: "#FEA44C"
#     blue: "#367BF0"
#     magenta: "#BF25ED"
#     cyan: "#49AEE6"
#     white: "#E6E6E6"

#   # Bright colors
#   bright:
#     black: "#8C42AB"
#     red: "#EC0101"
#     green: "#47D489"
#     yellow: "#FF8A18"
#     blue: "#277FFF"
#     magenta: "#D71655"
#     cyan: "#05A1F7"
#     white: "#FFFFFF"

window_opacity: 1.00

selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>\t"

  save_to_clipboard: false

cursor:
  style: Block

  unfocused_hollow: true

live_config_reload: true

# Shell
#
# You can set `shell.program` to the path of your favorite shell, e.g. `/bin/fish`.
# Entries in `shell.args` are passed unmodified as arguments to the shell.
#
# Default:
#   - (macOS) /bin/bash --login
#   - (Linux/BSD) user login shell
#   - (Windows) powershell
shell:
  program: /bin/bash
  #args:
  #  - --login

mouse:
  double_click: { threshold: 300 }
  triple_click: { threshold: 300 }

  hide_when_typing: false

  #url:
    # URL launcher
    #
    # This program is executed when clicking on a text which is recognized as a URL.
    # The URL is always added to the command as the last parameter.
    #
    # When set to `launcher: None`, URL launching will be disabled completely.
    #
    # Default:
    #   - (macOS) open
    #   - (Linux/BSD) xdg-open
    #   - (Windows) explorer
    #launcher:
    #  program: xdg-open
    #  args: []

    # URL modifiers
    #
    # These are the modifiers that need to be held down for opening URLs when clicking
    # on them. The available modifiers are documented in the key binding section.
    #modifiers: None

# Mouse bindings
#
# Mouse bindings are specified as a list of objects, much like the key
# bindings further below.
#
# To trigger mouse bindings when an application running within Alacritty captures the mouse, the
# `Shift` modifier is automatically added as a requirement.
#
# Each mouse binding will specify a:
#
# - `mouse`:
#
#   - Middle
#   - Left
#   - Right
#   - Numeric identifier such as `5`
#
# - `action` (see key bindings)
#
# And optionally:
#
# - `mods` (see key bindings)
mouse_bindings:
  - { mouse: Middle, action: PasteSelection }

# Key bindings
#
# Key bindings are specified as a list of objects. For example, this is the
# default paste binding:
#
# `- { key: V, mods: Control|Shift, action: Paste }`
#
# Each key binding will specify a:
#
# - `key`: Identifier of the key pressed
#
#    - A-Z
#    - F1-F24
#    - Key0-Key9
#
#    A full list with available key codes can be found here:
#    https://docs.rs/glutin/*/glutin/event/enum.VirtualKeyCode.html#variants
#
#    Instead of using the name of the keys, the `key` field also supports using
#    the scancode of the desired key. Scancodes have to be specified as a
#    decimal number. This command will allow you to display the hex scancodes
#    for certain keys:
#
#       `showkey --scancodes`.
#
# Then exactly one of:
#
# - `chars`: Send a byte sequence to the running application
#
#    The `chars` field writes the specified string to the terminal. This makes
#    it possible to pass escape sequences. To find escape codes for bindings
#    like `PageUp` (`"\x1b[5~"`), you can run the command `showkey -a` outside
#    of tmux. Note that applications use terminfo to map escape sequences back
#    to keys. It is therefore required to update the terminfo when changing an
#    escape sequence.
#
# - `action`: Execute a predefined action
#
#   - ToggleViMode
#   - Copy
#   - Paste
#   - PasteSelection
#   - IncreaseFontSize
#   - DecreaseFontSize
#   - ResetFontSize
#   - ScrollPageUp
#   - ScrollPageDown
#   - ScrollLineUp
#   - ScrollLineDown
#   - ScrollToTop
#   - ScrollToBottom
#   - ClearHistory
#   - Hide
#   - Minimize
#   - Quit
#   - ToggleFullscreen
#   - SpawnNewInstance
#   - ClearLogNotice
#   - ClearSelection
#   - ReceiveChar
#   - None
#
#   (`mode: Vi` only):
#   - Open
#   - Up
#   - Down
#   - Left
#   - Right
#   - First
#   - Last
#   - FirstOccupied
#   - High
#   - Middle
#   - Low
#   - SemanticLeft
#   - SemanticRight
#   - SemanticLeftEnd
#   - SemanticRightEnd
#   - WordRight
#   - WordLeft
#   - WordRightEnd
#   - WordLeftEnd
#   - Bracket
#   - ToggleNormalSelection
#   - ToggleLineSelection
#   - ToggleBlockSelection
#   - ToggleSemanticSelection
#
#   (macOS only):
#   - ToggleSimpleFullscreen: Enters fullscreen without occupying another space
#
#   (Linux/BSD only):
#   - CopySelection: Copies into selection buffer
#
# - `command`: Fork and execute a specified command plus arguments
#
#    The `command` field must be a map containing a `program` string and an
#    `args` array of command line parameter strings. For example:
#       `{ program: "alacritty", args: ["-e", "vttest"] }`
#
# And optionally:
#
# - `mods`: Key modifiers to filter binding actions
#
#    - Command
#    - Control
#    - Option
#    - Super
#    - Shift
#    - Alt
#
#    Multiple `mods` can be combined using `|` like this:
#       `mods: Control|Shift`.
#    Whitespace and capitalization are relevant and must match the example.
#
# - `mode`: Indicate a binding for only specific terminal reported modes
#
#    This is mainly used to send applications the correct escape sequences
#    when in different modes.
#
#    - AppCursor
#    - AppKeypad
#    - Alt
#
#    A `~` operator can be used before a mode to apply the binding whenever
#    the mode is *not* active, e.g. `~Alt`.
#
# Bindings are always filled by default, but will be replaced when a new
# binding with the same triggers is defined. To unset a default binding, it can
# be mapped to the `ReceiveChar` action. Alternatively, you can use `None` for
# a no-op if you do not wish to receive input characters for that binding.
#
# If the same trigger is assigned to multiple actions, all of them are executed
# at once.
key_bindings:
  #- { key: Paste,                                action: Paste          }
  #- { key: Copy,                                 action: Copy           }
  #- { key: L,         mods: Control,             action: ClearLogNotice }
  #- { key: L,         mods: Control, mode: ~Vi,  chars: "\x0c"          }
  #- { key: PageUp,    mods: Shift,   mode: ~Alt, action: ScrollPageUp,  }
  #- { key: PageDown,  mods: Shift,   mode: ~Alt, action: ScrollPageDown }
  #- { key: Home,      mods: Shift,   mode: ~Alt, action: ScrollToTop,   }
  #- { key: End,       mods: Shift,   mode: ~Alt, action: ScrollToBottom }

  # Vi Mode
  #- { key: Space,  mods: Shift|Control, mode: Vi, action: ScrollToBottom          }
  #- { key: Space,  mods: Shift|Control,           action: ToggleViMode            }
  #- { key: Escape,                      mode: Vi, action: ClearSelection          }
  #- { key: I,                           mode: Vi, action: ScrollToBottom          }
  #- { key: I,                           mode: Vi, action: ToggleViMode            }
  #- { key: Y,      mods: Control,       mode: Vi, action: ScrollLineUp            }
  #- { key: E,      mods: Control,       mode: Vi, action: ScrollLineDown          }
  #- { key: G,                           mode: Vi, action: ScrollToTop             }
  #- { key: G,      mods: Shift,         mode: Vi, action: ScrollToBottom          }
  #- { key: B,      mods: Control,       mode: Vi, action: ScrollPageUp            }
  #- { key: F,      mods: Control,       mode: Vi, action: ScrollPageDown          }
  #- { key: U,      mods: Control,       mode: Vi, action: ScrollHalfPageUp        }
  #- { key: D,      mods: Control,       mode: Vi, action: ScrollHalfPageDown      }
  #- { key: Y,                           mode: Vi, action: Copy                    }
  #- { key: Y,                           mode: Vi, action: ClearSelection          }
  #- { key: Copy,                        mode: Vi, action: ClearSelection          }
  #- { key: V,                           mode: Vi, action: ToggleNormalSelection   }
  #- { key: V,      mods: Shift,         mode: Vi, action: ToggleLineSelection     }
  #- { key: V,      mods: Control,       mode: Vi, action: ToggleBlockSelection    }
  #- { key: V,      mods: Alt,           mode: Vi, action: ToggleSemanticSelection }
  #- { key: Return,                      mode: Vi, action: Open                    }
  #- { key: K,                           mode: Vi, action: Up                      }
  #- { key: J,                           mode: Vi, action: Down                    }
  #- { key: H,                           mode: Vi, action: Left                    }
  #- { key: L,                           mode: Vi, action: Right                   }
  #- { key: Up,                          mode: Vi, action: Up                      }
  #- { key: Down,                        mode: Vi, action: Down                    }
  #- { key: Left,                        mode: Vi, action: Left                    }
  #- { key: Right,                       mode: Vi, action: Right                   }
  #- { key: Key0,                        mode: Vi, action: First                   }
  #- { key: Key4,   mods: Shift,         mode: Vi, action: Last                    }
  #- { key: Key6,   mods: Shift,         mode: Vi, action: FirstOccupied           }
  #- { key: H,      mods: Shift,         mode: Vi, action: High                    }
  #- { key: M,      mods: Shift,         mode: Vi, action: Middle                  }
  #- { key: L,      mods: Shift,         mode: Vi, action: Low                     }
  #- { key: B,                           mode: Vi, action: SemanticLeft            }
  #- { key: W,                           mode: Vi, action: SemanticRight           }
  #- { key: E,                           mode: Vi, action: SemanticRightEnd        }
  #- { key: B,      mods: Shift,         mode: Vi, action: WordLeft                }
  #- { key: W,      mods: Shift,         mode: Vi, action: WordRight               }
  #- { key: E,      mods: Shift,         mode: Vi, action: WordRightEnd            }
  #- { key: Key5,   mods: Shift,         mode: Vi, action: Bracket                 }

  # (Windows, Linux, and BSD only)
  - { key: V,        mods: Control|Shift,           action: Paste            }
  - { key: C,        mods: Control|Shift,           action: Copy             }
  #- { key: C,        mods: Control|Shift, mode: Vi, action: ClearSelection   }
  #- { key: Insert,   mods: Shift,                   action: PasteSelection   }
  #- { key: Key0,     mods: Control,                 action: ResetFontSize    }
  #- { key: Equals,   mods: Control,                 action: IncreaseFontSize }
  #- { key: Add,      mods: Control,                 action: IncreaseFontSize }
  #- { key: Subtract, mods: Control,                 action: DecreaseFontSize }
  #- { key: Minus,    mods: Control,                 action: DecreaseFontSize }

  # (Windows only)
  - { key: Return,   mods: Alt,           action: ToggleFullscreen }

  # (macOS only)
  #- { key: K,      mods: Command, mode: ~Vi, chars: "\x0c"            }
  #- { key: Key0,   mods: Command,            action: ResetFontSize    }
  #- { key: Equals, mods: Command,            action: IncreaseFontSize }
  #- { key: Add,    mods: Command,            action: IncreaseFontSize }
  #- { key: Minus,  mods: Command,            action: DecreaseFontSize }
  #- { key: K,      mods: Command,            action: ClearHistory     }
  #- { key: V,      mods: Command,            action: Paste            }
  #- { key: C,      mods: Command,            action: Copy             }
  #- { key: C,      mods: Command, mode: Vi,  action: ClearSelection   }
  #- { key: H,      mods: Command,            action: Hide             }
  #- { key: M,      mods: Command,            action: Minimize         }
  #- { key: Q,      mods: Command,            action: Quit             }
  #- { key: W,      mods: Command,            action: Quit             }
  #- { key: N,      mods: Command,            action: SpawnNewInstance }
  #- { key: F,      mods: Command|Control,    action: ToggleFullscreen }

debug:
  render_timer: false

  persistent_logging: false

  log_level: OFF

  print_events: false
