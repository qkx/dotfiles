;; ================================
;; Init config
;; ================================

;; Define custom file for Custom
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; Define and initialize package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; use-package to simpify config file
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure 't)

;; ================================
;; User Interface config
;; ================================

;; Remove unused bars
(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq visible-bell t)
(setq require-final-newline t)

;; Show matching parenthesis
(show-paren-mode 1)

;; Enable electric pair mode
(electric-pair-mode t)

;; Set command key as meta key
(setq mac-option-modifier nil
      mac-command-modifier 'meta)

;; Theme
(load-theme 'dichromacy t)

;; ================================
;; Font config
;; ================================

(set-face-attribute 'default nil :font "Fira Mono" :height 130)
(set-frame-font "Fira Mono" nil t)

;; ================================
;; ido config
;; ================================
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode t)

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; ================================
;; Auto completion config
;; ================================
(use-package company
  :config
  (setq company-idle-delay 0
	company-minimum-prefix-length 4
	company-selection-wrap-around t))
(global-company-mode)

;; ================================
;; Miscellaneous config
;; ================================

;; install try package to experiment with packages
(use-package try
  :ensure t)

;; Display key shortcuts
(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;; Sensible line breaking
(add-hook 'text-mode-hook 'visual-line-mode)

;; No tabs
(setq-default indent-tabs-mode nil)

;; Overwrite selected text
(delete-selection-mode t)

;; Disable backup files
(setq make-backup-files nil)

;; Magit
(use-package magit
  :ensure t)

;; ================================
;; org-mode configuration
;; ================================
(use-package org
  :bind
  (("C-c l" . org-store-link)
   ("C-c a" . org-agenda)
   ("C-c c" . org-capture)))
(setq org-log-done t)

;; Set org-directory and default target file for notes
(setq org-directory "~/Dropbox/org")
(setq org-default-notes-file (concat org-directory "/notes.org"))

;; Set org-agenda-files
(setq org-agenda-files '("~/Dropbox/org"))

;; Set refiling for other files
(setq org-refile-targets '((org-agenda-files :maxlevel . 3)))

;; Org-mode looks
(setq org-startup-indented t
      org-pretty-entities t
      org-hide-emphasis-markers t
      org-startup-with-inline-images t
      org-image-actual-width '(300))
(use-package org-superstar
  :config
  (setq org-superstar-special-todo-items t)
  (add-hook 'org-mode-hook (lambda ()
			     (org-superstar-mode 1))))

;; Show hidden emphasis markers
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; Skip repeater after deadline
(setq org-agenda-skip-scheduled-if-deadline-is-shown 'repeated-after-deadline)

;; ================================
;; ace-window config
;; ================================
(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "M-o") 'ace-window))

;; ================================
;; swiper config for searching
;; ================================
(use-package counsel
  :ensure t
  :config
  (ivy-mode t)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq enable-recursive-minibuffers t)
  ;; Ivy-based interface to standard commands
  (global-set-key (kbd "C-s") 'swiper-isearch)
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
  (global-set-key (kbd "M-y") 'counsel-yank-pop)
  (global-set-key (kbd "<f1> f") 'counsel-describe-function)
  (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
  (global-set-key (kbd "<f1> l") 'counsel-find-library)
  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
  (global-set-key (kbd "<f2> j") 'counsel-set-variable)
  (global-set-key (kbd "C-x b") 'ivy-switch-buffer)
  (global-set-key (kbd "C-c v") 'ivy-push-view)
  (global-set-key (kbd "C-c V") 'ivy-pop-view)
  (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history))

;; ================================
;; python dev config
;; ================================
;; elpy
(use-package elpy
  :ensure t
  :init
  (elpy-enable)
  :config
  (setq elpy-rpc-virtualenv-path 'current))

;; black
(use-package blacken
  :ensure t
  :hook (python-mode . blacken-mode))
