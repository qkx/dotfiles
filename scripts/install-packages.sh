#!/bin/bash

declare -a packages=(
    "vim"
    "curl"
    "git"
    "tmux"
)

sudo apt install -y ${packages[@]}
